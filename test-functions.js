const function1 = arr => {
    console.log('Входные данные', arr)
    arr.sort()

    let count = 0

    for (const index in arr) {
        const numIndex = parseInt(index)

        if (arr.length - 1 === numIndex) {
            continue
        }

        if (arr[numIndex + 1] - arr[numIndex] === 1) {
            count++
        }
    }

    if (count === arr.length - 1) {
        console.log('   > Числа последовательные')
        return true
    } else {
        console.log('   > Числа не последовательные')
        return false
    }
}

const function2 = arr => {
    console.log('Входные данные', arr)
    
    const newArr = []
    
    for (const element of arr) {
        const search = newArr.filter(a => a === element)
        
        if (search.length > 0) {
            continue
        }
        
        newArr.push(element)
    }
    
    console.log('newArr', newArr.sort())
    return newArr.sort()
}

const function3 = arr => {
    console.log('Входные данные', arr)

    const newArr = []

    for (const element of arr) {
        const isExist = newArr.find(item => {
            const keys = Object.keys(item)
            return parseInt(keys[0]) === element
        })

        if (isExist) {
            continue
        }

        const search = arr.filter(a => a === element)
        const newElement = {}
        newElement[element] = search.length

        newArr.push(newElement)
    }

    console.log('   > Результат', newArr);
    return newArr

}


console.log('Написать функцию которая: учитывает массив несортированных чисел и определяет являются ли числа в массиве последовательными')
function1([5, 2, 3, 1, 4]);
function1([34, 23, 52, 12, 3]);
function1([7, 6, 5, 5, 3, 4]);

console.log('\n', )
console.log('Реализовать функцию, которая принимает этот массив в качестве аргумента и возвращает новый массив, содержащий только уникальные элементы. Новый массив должен быть отсортирован по возрастанию.')
function2([1, 3, 2, 2, 4, 3, 5, 6, 5])
function2([9, 9, 9, 9, 9])
function2([1, 2, 3, 4, 5])


console.log('\n', )
console.log('Написать функцию которая принимает в качестве аргумента массив чисел и возвращает новый массив с количеством повторений первоначального массива {число: кол во повторений}')
function3([1, 3, 2, 2, 4, 3, 5, 6, 5])