import Vuex from 'vuex'

const store = new Vuex.Store({
    state: {
        user: {
            login: ''
        },

        cart: [],

        restaurants: [
            {
                id: 'pizza-plus',
                name: 'Пицца плюс',
                deliveryTime: '50 мин',
                averageRating: 4.5,
                minimalOrder: 900,
                category: ['Пицца']
            },
            {
                id: 'tanuki',
                name: 'Тануки',
                deliveryTime: '60 мин',
                averageRating: 4.5,
                minimalOrder: 1200,
                category: ['Суши', 'Роллы']
            },
            {
                id: 'food-band',
                name: 'FoodBand',
                deliveryTime: '40 мин',
                averageRating: 4.5,
                minimalOrder: 450,
                category: ['Пицца']
            },
            {
                id: 'palki-skalki',
                name: 'Палки скалки',
                deliveryTime: '55 мин',
                averageRating: 4.5,
                minimalOrder: 500,
                category: ['Пицца']
            },
            {
                id: 'gusi-lebedi',
                name: 'Гуси Лебеди',
                deliveryTime: '75 мин',
                averageRating: 4.5,
                minimalOrder: 1000,
                category: ['Русская кухня']
            },
            {
                id: 'pizza-burger',
                name: 'PizzaBurger',
                deliveryTime: '45 мин',
                averageRating: 4.5,
                minimalOrder: 700,
                category: ['Пицца']
            }
        ],

        goods: [
            {
                id: 'pizza-plus/pizza-vesuvius',
                restaurantId: 'pizza-plus',
                name: 'Пицца Везувий',
                ingredients: ['Соус томатный', 'сыр «Моцарелла»', 'ветчина', 'пепперони', 'перец «Халапенье»', 'соус «Тобаско»', 'томаты'],
                price: 545,
            },
            {
                id: 'pizza-plus/pizza-girls',
                restaurantId: 'pizza-plus',
                name: 'Пицца Девичник',
                ingredients: ['Соус томатный', 'постное тесто', 'нежирный сыр', 'кукуруза', 'лук', 'маслины', 'грибы', 'помидоры', 'болгарский перец'],
                price: 450,
            },
            {
                id: 'pizza-plus/pizza-oleole',
                restaurantId: 'pizza-plus',
                name: 'Пицца Оле-Оле',
                ingredients: ['Соус томатный', 'сыр «Моцарелла»', 'черри', 'маслины', 'зелень', 'майонез'],
                price: 440,
            },
            {
                id: 'pizza-plus/pizza-plus',
                restaurantId: 'pizza-plus',
                name: 'Пицца Плюс',
                ingredients: ['Соус томатный', 'сыр «Моцарелла»', 'сыр «Чеддер»', 'томаты', 'пепперони', 'телятина', 'грибы', 'бекон', 'болгарский перец'],
                price: 805,
            },
            {
                id: 'pizza-plus/pizza-hawaiian',
                restaurantId: 'pizza-plus',
                name: 'Пицца Гавайская',
                ingredients: ['Соус томатный', 'сыр «Моцарелла»', 'ветчина', 'ананасы'],
                price: 440,
            },
            {
                id: 'pizza-plus/pizza-classic',
                restaurantId: 'pizza-plus',
                name: 'Пицца Классика',
                ingredients: ['Соус томатный', 'сыр «Моцарелла»', 'сыр «Пармезан»', 'ветчина', 'салями', 'грибы'],
                price: 510,
            },

            {
                id: 'tanuki/azhi',
                restaurantId: 'tanuki',
                name: 'Амай',
                ingredients: ['Тунец', 'Лосось', 'Лист Салата', 'Микрозелень', 'Огурец', 'Авокадо', 'Сыр Филадельфия'],
                price: 126,
            },
            {
                id: 'tanuki/black',
                restaurantId: 'tanuki',
                name: 'Аристократ',
                ingredients: ['Угорь', 'Тигровая креветка', 'Лосось', 'Икра Капеллана', 'Авокадо', 'Сыр Филадельфия'],
                price: 220,
            },
            {
                id: 'tanuki/fresh',
                restaurantId: 'tanuki',
                name: 'Дабл космос',
                ingredients: ['Тунец', 'Лосось', 'Икра Капеллана', 'Огурец', 'Авокадо', 'Сыр Филадельфия'],
                price: 100,
            },
            {
                id: 'tanuki/nisuaz',
                restaurantId: 'tanuki',
                name: 'Изи',
                ingredients: ['Тигровая креветка', 'Кальмар', 'Икра Капеллана', 'Огурец', 'Японский майонез'],
                price: 250,
            },
            {
                id: 'tanuki/smoke',
                restaurantId: 'tanuki',
                name: 'Калифорния с креветкой',
                ingredients: ['Тигровая креветка', 'Лосось', 'Икра Капеллана', 'Огурец', 'Сыр Филадельфия'],
                price: 160,
            },
            {
                id: 'tanuki/tanuki',
                restaurantId: 'tanuki',
                name: 'Красный дракон',
                ingredients: ['Кальмар', 'Лосось', 'Авокадо', 'Сыр Филадельфия'],
                price: 90,
            },

            {
                id: 'food-band/margarita',
                restaurantId: 'food-band',
                name: 'Маргарита',
                ingredients: ['Пицца соус', 'сыр моцарелла', 'свежий помидор'],
                price: 100,
            },
            {
                id: 'food-band/meet',
                restaurantId: 'food-band',
                name: 'Мясная',
                ingredients: ['Пицца соус', 'сыр моцарелла', 'колбаски пепперони', 'бекон', 'свинина', 'сочная курочка', 'свежий помидор'],
                price: 200,
            },
            {
                id: 'food-band/norwegian',
                restaurantId: 'food-band',
                name: 'Королева моря',
                ingredients: ['Пицца соус', 'сыр моцарелла', 'лосось', 'креветка', 'маслины', 'болгарский перец', 'зеленый лук'],
                price: 350,
            },
            {
                id: 'food-band/pepperoni',
                restaurantId: 'food-band',
                name: 'Пепперони',
                ingredients: ['Пицца соус', 'сыр моцарелла', 'колбаски пепперони'],
                price: 250,
            },
            {
                id: 'food-band/seven-cheeses',
                restaurantId: 'food-band',
                name: 'Сливочная',
                ingredients: ['белый соус', 'сыр моцарелла'],
                price: 200,
            },
            {
                id: 'food-band/tom-yam',
                restaurantId: 'food-band',
                name: 'Том Ям',
                ingredients: ['Соус Том Ям', 'сыр моцарелла', 'креветка', 'сочная курочка', 'свежий помидор', 'шампиньоны'],
                price: 300,
            },

            {
                id: 'palki-skalki/burrito',
                restaurantId: 'palki-skalki',
                name: 'Буррито',
                ingredients: ['Курица', 'Лаваш', 'Майонез', 'Помидор', 'Огурец'],
                price: 100,
            },
            {
                id: 'palki-skalki/cheeseburger',
                restaurantId: 'palki-skalki',
                name: 'Чизбургер',
                ingredients: ['Куриная котлета', 'сыр', 'салат', 'помидор', 'кетчуп'],
                price: 70,
            },
            {
                id: 'palki-skalki/combo',
                restaurantId: 'palki-skalki',
                name: 'Сет три пиццы',
                ingredients: ['пеперони', 'сыр', 'кетчуп', 'тесто', 'мясо', 'огурец', 'макароны'],
                price: 260,
            },
            {
                id: 'palki-skalki/fusion',
                restaurantId: 'palki-skalki',
                name: 'Пицца Фьюжн',
                ingredients: ['15 кошек', '17 блошек', 'охапка дров'],
                price: 130,
            },
            {
                id: 'palki-skalki/rome',
                restaurantId: 'palki-skalki',
                name: 'Пиица 4 Рима',
                ingredients: ['макароны', 'пармезан', 'яйцо', 'вино'],
                price: 150,
            },
            {
                id: 'palki-skalki/udon',
                restaurantId: 'palki-skalki',
                name: 'Удон',
                ingredients: ['креветка', 'грибы', 'лапша', 'вода'],
                price: 90,
            },

            {
                id: 'gusi-lebedi/calf-sauce',
                restaurantId: 'gusi-lebedi',
                name: 'Пюрешка',
                ingredients: ['Грибы', 'картошка', 'огурец'],
                price: 40,
            },
            {
                id: 'gusi-lebedi/chick',
                restaurantId: 'gusi-lebedi',
                name: 'Шашлык',
                ingredients: ['Курица'],
                price: 70,
            },
            {
                id: 'gusi-lebedi/chicken-soup',
                restaurantId: 'gusi-lebedi',
                name: 'Супчик',
                ingredients: ['Лапша', 'бульон', 'яйцо'],
                price: 30,
            },
            {
                id: 'gusi-lebedi/dumplings',
                restaurantId: 'gusi-lebedi',
                name: 'Пельмени',
                ingredients: ['Тесто', 'курица', 'сметана'],
                price: 40,
            },
            {
                id: 'gusi-lebedi/ear',
                restaurantId: 'gusi-lebedi',
                name: 'Супчик 2',
                ingredients: ['Лапша', 'бульон', 'яйцо'],
                price: 35,
            },
            {
                id: 'gusi-lebedi/plov',
                restaurantId: 'gusi-lebedi',
                name: 'Плов',
                ingredients: ['13 кошек', '12 блошек', 'охапка дров', 'секретный ингридиент'],
                price: 90,
            },
            {
                id: 'gusi-lebedi/squid',
                restaurantId: 'gusi-lebedi',
                name: 'Кальмар с лимоном',
                ingredients: ['Кальмар', 'лимон'],
                price: 200,
            },
            {
                id: 'gusi-lebedi/pig-chop',
                restaurantId: 'gusi-lebedi',
                name: 'Свинка',
                ingredients: ['свинина', 'сыр'],
                price: 150,
            },
            {
                id: 'gusi-lebedi/zander',
                restaurantId: 'gusi-lebedi',
                name: 'Отбивная с пюре',
                ingredients: ['курица', 'картошка', 'сметана'],
                price: 90,
            },

            {
                id: 'pizza-burger/pizza-caesar',
                restaurantId: 'pizza-burger',
                name: 'Аллерона',
                ingredients: ['Сливочный соус', 'Сыр Моцарелла', 'Индейка', 'Клюква', 'Апельсин'],
                price: 109,
            },
            {
                id: 'pizza-burger/pizza-chef',
                restaurantId: 'pizza-burger',
                name: 'Аррива',
                ingredients: ['Красный соус', 'Сыр Моцарелла', 'Помидоры', 'Каперсы', 'Орегано', 'Прошутто', 'Рукола'],
                price: 120,
            },
            {
                id: 'pizza-burger/pizza-dacha',
                restaurantId: 'pizza-burger',
                name: 'Ассорти',
                ingredients: ['Красный соус', 'Сыр Моцарелла', 'Помидор', 'Болгарский перец', 'Копченая куриная грудка', 'Охотничьи колбаски', 'Лук', 'Бекон'],
                price: 115,
            },
            {
                id: 'pizza-burger/pizza-meat',
                restaurantId: 'pizza-burger',
                name: 'Бавария',
                ingredients: ['Красный соус', 'Сыр Моцарелла', 'Салями', 'Охотничьи колбаски', 'Бекон', 'Лук маринованный', 'Перец Чили'],
                price: 100,
            },
            {
                id: 'pizza-burger/pizza-pepperoni',
                restaurantId: 'pizza-burger',
                name: 'Верона',
                ingredients: ['Сливочный соус', 'Грибы свежие', 'Сыр Моцарелла', 'Ветчина', 'Соус Французский'],
                price: 80,
            },
            {
                id: 'pizza-burger/pizza-village',
                restaurantId: 'pizza-burger',
                name: 'Гралья',
                ingredients: ['Сливочный соус', 'Сыр Моцарелла', 'Помидор', 'Тигровые креветки', 'Оливки', 'Сыр Пармезан', 'Рукола'],
                price: 150,
            }
        ],
    },

    getters: {
        getUserLogin: state => state.user.login,

        getCartList: state => state.cart,

        getRestaurantsList: state => state.restaurants,

        getRestaurantById: state => id => {
            return state.restaurants.find(item => item.id === id)
        },

        getRestaurantsByNameAndCategory: state => query => {
            return state.restaurants.filter(item => {
                const itemData = `${item['name']} ${item['category'].join(', ')}`.toLowerCase()
                return itemData.indexOf(query.toLowerCase()) >= 0
                }
            )
        },

        getGoodsList: state => state.goods,

        getGoodsListById: state => id => {
            return state.goods.find(item => item.id === id)
        },

        getGoodsListByRestaurantId: state => id => {
            return state.goods.filter(item => item.restaurantId === id)
        },

        getGoodsByNameAndIngredients: state => query => {
            return state.goods.filter(item => {
                    const itemData = `${item['name']} ${item['ingredients'].join(', ')}`.toLowerCase()
                    return itemData.indexOf(query.toLowerCase()) >= 0
                }
            )
        },
    },

    mutations: {
        login(state, payload) {
            state.user.login = payload
        },

        logout(state) {
            state.user.login = '';
        },

        restoreCart(state, payload) {
            state.cart.push(payload)
        },

        addToCart(state, id) {
            const item = state.cart.find(item => item.id === id)

            if (item) {
                return
            }

            state.cart.push({
                id,
                quantity: 1
            })
        },

        addQuantity(state, id) {
            const cartItem = state.cart.find(item => item['id'] === id)
            cartItem['quantity']++
        },

        removeQuantity(state, id) {
            const cartItem = state.cart.find(item => item['id'] === id)

            cartItem['quantity']--

            if (cartItem['quantity'] < 1) {
                state.cart = state.cart.filter(item => item['id'] !== id)
            }
        }
    },

    actions: {
        doLogin({ commit }, payload) {
            commit('login', payload)
        },

        logout({ commit }) {
            commit('logout')
        },

        restoreCart({ commit }, paload) {
            commit('restoreCart', paload)
        },

        addToCart({ commit }, payload) {
            commit('addToCart', payload)
        },

        addQuantity({ commit }, payload) {
            commit('addQuantity', payload);
        },

        removeQuantity({ commit }, payload) {
            commit('removeQuantity', payload);
        }
    }
})

export default store;