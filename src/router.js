import { createRouter, createWebHistory } from 'vue-router'

import HomeViews from "@/views/HomeViews.vue";
import RestaurantViews from "@/views/RestaurantViews.vue";
import ComingSoonViews from "@/views/ComingSoonViews.vue";

const routes = [
    {
        path: '/',
        name: 'home',
        component: HomeViews
    },
    {
        path: '/restaurant/:id',
        name: 'restaurant',
        component: RestaurantViews
    },
    {
        path: '/coming-soon',
        name: 'coming-soon',
        component: ComingSoonViews
    }
]

const router = createRouter({
    // 4. Provide the history implementation to use. We are using the hash history for simplicity here.
    history: createWebHistory(),
    routes, // short for `routes: routes`

    scrollBehavior() {
        return {
            behavior: 'smooth',
            top: 0
        }
    }
})

export default router