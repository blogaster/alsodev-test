import { createApp } from 'vue'
import router from "@/router";
import store from "@/store";
import mitt from 'mitt'
import Notifications from '@kyvg/vue3-notification'

import App from './App.vue'

import './css/normalize.css'
import './css/style.css'


const emitter = mitt()
const app = createApp(App)

app.use(router)
app.use(store)
app.use(Notifications)

app.config.globalProperties.$emitter = emitter
app.mount('#app')