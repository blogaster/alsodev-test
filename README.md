# alsodev-test

### Устанавливаем зависимости
```
npm install
```

### Запустить локальный сервер
```
npm run serve
```

### Собрать проект для продакшена
```
npm run build
```

### Запустить выполнение второй части тестового задания
```
node test-functions.js
```
